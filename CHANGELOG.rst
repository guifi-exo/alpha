=========
Changelog
=========

Version 0.1.0
=============

- Basic features added
- More changes will be included soon!!

Version 0.2.0
=============

- New scheme of data structures to allocate extra data
- Selection of level of displayed data from attributes (-e, --extra-info)
- More changes will be included soon!!

