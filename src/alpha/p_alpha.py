import argparse
import sys

from alpha import __version__
from alpha.parse import build_json
from alpha.members import CostSource, Income
from alpha.utils import *

__author__ = "Victor Oncins"
__copyright__ = "Victor Oncins"
__license__ = "GPLv3"

_logger = logging.getLogger(__name__)


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Traffic and financial analysis")
    parser.add_argument(
        "--version",
        action="version",
        version="alpha {ver}".format(ver=__version__))
    parser.add_argument(
        "-a",
        "--attributes-file",
        dest="a",
        help="attributes JSON file",
        type=str,
        metavar="TF")
    parser.add_argument(
        "-m",
        "--member-file",
        dest="m",
        help="member source JSON file",
        type=str,
        metavar="MF")
    parser.add_argument(
        "-f",
        "--filter-string",
        dest="f",
        help="filtering match string",
        type=str,
        metavar="FL")
    parser.add_argument(
        "-k",
        "--filter-attr",
        dest="k",
        help="filtering attribute match key",
        type=str,
        metavar="KY")
    parser.add_argument(
        dest="o",
        help="command",
        type=str,
        metavar="COMMAND")
    parser.add_argument(
        "-e",
        "--extra-info",
        dest="e",
        help="display extra info from attributes of members",
        action="store_true")
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO)
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG)
    return parser.parse_args(args)


def parse_commands(args):
    _member_l = build_json(args.a, args.m)

    if not _member_l:
        _logger.info("Empty member list. Job is done!")
        return

    _attr_l = find_attributes(args.k, args.f, _member_l)

    if args.o == "PrintMember":
        _tot_cost = 0
        _tot_income = 0
        _part_cost = 0
        _part_income = 0

        for m in _member_l:
            _matched_attr = list()

            if _attr_l:
                _matched_attr.extend([a for a in m.cost_syme.object_list if a in _attr_l])
                _matched_attr.extend([a for a in m.income_syme.object_list if a in _attr_l])
                _matched_attr.extend([a for a in m.traffic_syme.object_list if a in _attr_l])

            if _matched_attr:
                _part_cost = m.cost_syme.sum
                _part_income = m.income_syme.sum

                if args.e:
                    _extra_info = ','.join('[{:<.10}]'.format(str(a)) for a in _matched_attr)
                else:
                    _extra_info = '[<hidden>]'

                _d = get_format_row(na=str(m),
                                    tc=_part_cost,
                                    ic=_part_income,
                                    bl=_part_income - _part_cost,
                                    ei=_extra_info)

                if (_part_income - _part_cost) >= 0:
                    print_row(_d, '{na}{tc}{ic}{bl}{ei}', 'green', '')
                else:
                    print_row(_d, '{na}{tc}{ic}{bl}{ei}', 'red', '')

                _tot_cost += _part_cost
                _tot_income += _part_income

        _d = get_format_row(na='Total',
                            tc=_tot_cost,
                            ic=_tot_income,
                            bl=_tot_income - _tot_cost)

        print_row(_d, '{na}{tc}{ic}{bl}', 'yellow', 'reverse')

    elif args.o == "PrintCostAttr":
        _tot_cost = 0
        for r in _member_l.attribute_list:
            if type(r) == CostSource and r in _attr_l:
                if args.e:
                    _extra_info = ','.join('[{:<.20}]'.format(str(a)) for a in [r.attrib_group, r.value_expr])
                else:
                    _extra_info = '[<hidden>]'

                _d = get_format_row(na=str(r),
                                    ac=r.financial_syme.sum,
                                    mm=r.mapped_members,
                                    ei=_extra_info)

                _tot_cost += r.financial_syme.sum

                print_row(_d, '{na}{ac}{mm}{ei}', 'green', '')

        _d = get_format_row(na='Total',
                            ac=_tot_cost)
        print_row(_d, '{na}{ac}', 'yellow', 'reverse')

    elif args.o == "PrintIncAttr":
        _tot_inc = 0
        for r in _member_l.attribute_list:
            if type(r) == Income and r in _attr_l:
                if args.e:
                    _extra_info = ','.join('[{:<.20}]'.format(str(a)) for a in [r.attrib_group, r.value_expr])
                else:
                    _extra_info = '[<hidden>]'

                _d = get_format_row(na=str(r),
                                    ic=r.financial_syme.sum,
                                    mm=r.mapped_members,
                                    ei=_extra_info)

                _tot_inc += r.financial_syme.sum

                print_row(_d, '{na}{ic}{mm}{ei}', 'green', '')

        _d = get_format_row(na='Total',
                            ic=_tot_inc)
        print_row(_d, '{na}{ic}', 'yellow', 'reverse')


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    parse_commands(args)
    _logger.info("Program ends here")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
