import logging
import re

from termcolor import colored

from alpha.constants import *

_logger = logging.getLogger(__name__)


# Evaluation postfix arithmetic expressions
def eval_postfix(_attrib, _member):
    if not _attrib.value_expr:
        return 0

    tokens = _attrib.value_expr.split()
    stack = []

    for tk in tokens:
        if tk in pfx_ops:
            try:
                b = stack.pop()
                a = stack.pop()
                assert b != 0
                stack.append(pfx_ops[tk](a, b))
            except (AssertionError, IndexError) as e:
                stack.append(0)
                _logger.error('Parsing error: {}'.format(e))

        elif tk == "n":
            stack.append(_attrib.mapped_members)
        elif tk == "t":
            stack.append(_member.traffic_syme.sum)
        elif tk == "T":
            stack.append(_attrib.traffic_syme.sum)
        else:
            try:
                stack.append(float(tk))
            except ValueError:
                _logger.error('The expression <{}> contains invalid token <{}>. '
                              'Members number is taken'.format(_attrib.value_expr, str(tk)))
                stack.append(_attrib.mapped_members)

    return stack.pop()


# Create a list of Members Attributes that match by keys
def find_attributes(key, val_reg_exp, member_l):

    _attr_l = list()

    if not key:
        return member_l.attribute_list

    try:
        _key_val = match_keys[key]
        _r = re.compile(val_reg_exp)
    except Exception as e:
        _logger.error('Incorrect regular expression or invalid key value: {}'.format(e))
        return _attr_l

    for _a in member_l.attribute_list:
        if _r.match(getattr(_a, _key_val)):
            _attr_l.append(_a)

    return _attr_l


# Formatted print
def print_row(row_dct, order='', color='', attr=''):
    try:
        if attr:
            print(colored(order.format(**row_dct), color, attrs=[attr]), end='')
        else:
            print(colored(order.format(**row_dct), color), end='')

        print('\n', end='')
    except (KeyError, AttributeError) as e:
        _logger.error('Key {} not found or related error arised'.format(e))


def get_format_row(**kwargs):
    _d = dict()
    _s = ''

    for k in kwargs.keys():
        if k in format_string_keys:
            _d[k] = format_string_keys[k].format(kwargs[k])

    return _d