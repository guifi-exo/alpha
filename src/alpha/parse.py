import json
from alpha.members import *

_logger = logging.getLogger(__name__)


# Open data source files
def build_json(a=None, m=None):
    try:
        with open(a) as _ad, open(m) as _md:
            _attr_json = json.load(_ad)
            _memb_json = json.load(_md)

            del _ad, _md

    except (IOError, json.JSONDecodeError, TypeError) as e:
        _logger.error('{}'.format(e))
        return

    _member_l = members_parse(_memb_json, _attr_json)
    _member_l.set_member_distr("traffic_analyze")
    _member_l.set_member_distr("non_traffic_analyze")

    return _member_l


def core_parse(dict_d):
    _attr_l = []

    if JSON_ATTRIB_NAME_TOKEN in dict_d:
        _name = str(dict_d.get(JSON_ATTRIB_NAME_TOKEN, ''))

        if JSON_ATTRIB_GROUP_TOKEN in dict_d:
            _attr_group = str(dict_d.get(JSON_ATTRIB_GROUP_TOKEN, ''))
        else:
            _attr_group = COST_GROUP_MEMBER
            _logger.debug(
                'Token <{}> not found. Attribute group <{}> is taken'.format(str(JSON_ATTRIB_GROUP_TOKEN),
                                                                             str(COST_GROUP_MEMBER)))
        if JSON_ATTRIB_EXPR_TOKEN in dict_d:
            _val_expr = str(dict_d.get(JSON_ATTRIB_EXPR_TOKEN, 0))
        else:
            _val_expr = ''
            _logger.debug(
                'Token <{}> not found. Empty expression is taken'.format(str(JSON_ATTRIB_EXPR_TOKEN)))

        return [_name, _attr_group, _val_expr]

    else:
        _logger.debug('Token <{}> not found. Dictionary item is ignored'.format(str(JSON_ATTRIB_NAME_TOKEN)))

    return _attr_l


def attribute_parse(coa, json_d):
    _attr_l = list()

    if not json_d:
        _logger.debug('The list of attributes is empty. No objects created')
        return _attr_l

    for dct in json_d:
        if JSON_ATTRIB_CLASS_TOKEN in dct:
            _coa = str(dct.get(JSON_ATTRIB_CLASS_TOKEN, ''))

            if coa == _coa:
                _attr_args = core_parse(dct)
                if _coa == COST_ATTRIB_CLASS:
                    _attr_l.append(CostSource(*_attr_args))
                elif _coa == TRAFFIC_ATTRIB_CLASS:
                    _attr_l.append(Traffic(*_attr_args))
                elif _coa == INCOME_ATTRIB_CLASS:
                    _attr_l.append(Income(*_attr_args))

    return _attr_l


# Parse a json file descriptor and create a new member list
def members_parse(_memb_json_d, _attr_json):
    if not isinstance(_memb_json_d, list):
        raise TypeError('The argument is {} class instead of list'.format(_memb_json_d.__class__.__name__))

    _cost_src_l = attribute_parse(COST_ATTRIB_CLASS, _attr_json)
    _traffic_l = attribute_parse(TRAFFIC_ATTRIB_CLASS, _attr_json)
    _income_l = attribute_parse(INCOME_ATTRIB_CLASS, _attr_json)
    _member_l = MemberList(_cost_src_l, _traffic_l, _income_l)

    if not _memb_json_d:
        _logger.debug('The list of members is empty. No member objects created')
        return _member_l

    for dct in _memb_json_d:
        if JSON_MEMBER_FIRSTNAME_TOKEN in dct:
            _firstname = str(dct.get(JSON_MEMBER_FIRSTNAME_TOKEN, ''))

            if JSON_MEMBER_LASTNAME_TOKEN in dct:
                _lastname = str(dct.get(JSON_MEMBER_LASTNAME_TOKEN, ''))
            else:
                _lastname = ''
                _logger.debug('Token <{}> not found. Empty lastname is taken'.format(str(JSON_MEMBER_LASTNAME_TOKEN)))

            if JSON_MEMBER_COST_GROUP_TOKEN in dct:
                _glist_value = list(dct.get(JSON_MEMBER_COST_GROUP_TOKEN, ''))
            else:
                _glist_value = list()
                _logger.debug('Token <{}> not found. Empty group list is taken'.format(str(JSON_MEMBER_COST_GROUP_TOKEN)))

            if JSON_MEMBER_TRAF_GROUP_TOKEN in dct:
                _tlist_value = list(dct.get(JSON_MEMBER_TRAF_GROUP_TOKEN, ''))
            else:
                _tlist_value = list()

            if JSON_MEMBER_INCOME_GROUP_TOKEN in dct:
                _ilist_value = list(dct.get(JSON_MEMBER_INCOME_GROUP_TOKEN, ''))
            else:
                _ilist_value = list()

            _member = Member(_firstname, _lastname)
            _member_l.append(_member)

            try:
                _member.append_attribute(_cost_src_l, _glist_value)
                _member.append_attribute(_traffic_l, _tlist_value)
                _member.append_attribute(_income_l, _ilist_value)
            except AttributeError as e:
                _logger.debug('Attribute can not be attached to member object {}: {}'.format(str(_member), e))

        else:
            _logger.debug('Token <{}> not found. Dictionary item is ignored'.format(str(JSON_MEMBER_FIRSTNAME_TOKEN)))

    return _member_l
