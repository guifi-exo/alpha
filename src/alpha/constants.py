# Global constants

JSON_ATTRIB_CLASS_TOKEN = 'class'
JSON_ATTRIB_NAME_TOKEN = 'name'
JSON_ATTRIB_GROUP_TOKEN = 'attribute_group'
JSON_ATTRIB_EXPR_TOKEN = 'value_expression'

JSON_MEMBER_FIRSTNAME_TOKEN = 'firstname'
JSON_MEMBER_LASTNAME_TOKEN = 'lastname'
JSON_MEMBER_COST_GROUP_TOKEN = 'cost_group_list'
JSON_MEMBER_TRAF_GROUP_TOKEN = 'traffic_group_list'
JSON_MEMBER_INCOME_GROUP_TOKEN = 'income_group_list'

COST_ATTRIB_CLASS = 'cost'
TRAFFIC_ATTRIB_CLASS = 'traffic'
INCOME_ATTRIB_CLASS = 'income'

COST_GROUP_MEMBER = 'Member'
COST_GROUP_SUBSCRIBER = 'Subscriber'
COST_GROUP_DEFAULT_NAME = 'Dummy'

DEFAULT_FIRSTNAME = 'Forocoches'

# Global variables
pfx_ops = {
    "+": (lambda a, b: a + b),
    "-": (lambda a, b: a - b),
    "*": (lambda a, b: a * b),
    "/": (lambda a, b: a / b)
}

match_keys = {
    'name': 'name',
    'attribute_group': 'attrib_group',
    'attribute_class': 'attrib_class'
}

format_string_keys = {
    'na': 'Name = {:<30.30} ', #Name
    'mc': 'MC = {:>8.3f} € ', #Membership cost
    'sc': 'SC = {:>8.3f} € ', #Service cost
    'tc': 'TC = {:>8.3f} € ', #Total member cost
    'ic': 'IC = {:>8.3f} € ', #Income
    'bl': 'BL = {:>8.3f} € ', #Balance
    'ac': 'AC = {:>8.3f} € ', #Attribute cost
    'at': 'AT = {:>8.3f} Mbps ', #Associatted traffic
    'ei': 'EI = {}', #Extra info
    'mm': 'MM = {:>5d} ', #Mapped members
    'ag': 'AG = {:<30.30} ' #Attribute group name
}