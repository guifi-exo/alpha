import logging
from alpha.constants import *
from alpha.utils import eval_postfix

_logger = logging.getLogger(__name__)


def check_attribute_list(inner_func):
    def iterate_wrapper(self, *args):
        if not self.attribute_list:
            return

        return inner_func(self, *args)

    return iterate_wrapper


class MemberAttribute:
    def __init__(self, name='', attrib_group='', value_expr=''):
        self.name = name
        self.attrib_group = attrib_group
        self.value_expr = value_expr
        self.mapped_members = 0

    def __str__(self):
        return str(self.name)

    def mapped_add(self, _val=0):
        self.mapped_members += _val


class CostSource(MemberAttribute):
    def __init__(self, name='', attrib_group='', value_expr=''):
        super().__init__(name, attrib_group, value_expr)
        self.attrib_class = COST_ATTRIB_CLASS
        self.traffic_syme = TransitSymbolEntry()
        self.financial_syme = SymbolEntry()


class Income(MemberAttribute):
    def __init__(self, name='', attrib_group='', value_expr=''):
        super().__init__(name, attrib_group, value_expr)
        self.attrib_class = INCOME_ATTRIB_CLASS
        self.traffic_syme = TransitSymbolEntry()
        self.financial_syme = SymbolEntry()


class Traffic(MemberAttribute):
    def __init__(self, name='', attrib_group='', value_expr=''):
        super().__init__(name, attrib_group, value_expr)
        self.attrib_class = TRAFFIC_ATTRIB_CLASS


class MemberList(list):
    def __init__(self, *args):
        super().__init__()
        self.attribute_list = list()
        self.extend_attribute_list(*args)

    def extend_attribute_list(self, *args):
        for l in args:
            if l:
                self.attribute_list.extend(l)
                
    @check_attribute_list
    def set_member_distr(self, meth_name=''):
        for m in self:
            try:
                getattr(m, meth_name)()
            except AttributeError as e:
                _logger.error('{}'.format(e))


class Member:
    def __init__(self, firstname=DEFAULT_FIRSTNAME, lastname=''):
        self.firstname = firstname
        self.lastname = lastname
        self.attribute_list = list()
        self.traffic_syme = TransitSymbolEntry()
        self.cost_syme = SymbolEntry()
        self.income_syme = SymbolEntry()

    def __str__(self):
        if self.lastname:
            return str(self.firstname) + ' ' + str(self.lastname)

        return str(self.firstname)

    def append_attribute(self, _ref_attribute_l, _attrib_group_l):
        if not _ref_attribute_l:
            return

        for a in _ref_attribute_l:
            if a.attrib_group in _attrib_group_l:
                self.attribute_list.append(a)
                a.mapped_add(1)

    @check_attribute_list
    def traffic_analyze(self):
        # Setting member local traffic load
        for a in self.attribute_list:
            if type(a) is Traffic:
                self.traffic_syme.add_symbol(eval_postfix(a, self), a)

        # Setting attribute aggregate traffic load
        for a in self.attribute_list:
            if type(a) is not Traffic:
                a.traffic_syme.extend_symbol(self.traffic_syme)

    @check_attribute_list
    def non_traffic_analyze(self):
        while self.attribute_list:
            a = self.attribute_list.pop(0)
            val = eval_postfix(a, self)
            if type(a) is CostSource:
                self.cost_syme.add_symbol(val, a)
                a.financial_syme.add_symbol(val, self)
            elif type(a) is Income:
                self.income_syme.add_symbol(val, a)
                a.financial_syme.add_symbol(val, self)
            elif type(a) is Traffic:
                pass
            else:
                _logger.debug('Unexpected attribute class: {}'.format(type(a).__name__))
                break


class SymbolEntry:
    def __init__(self):
        self.value_list = list()
        self.object_list = list()

    def add_value(self, value):
        try:
            float(value)
            self.value_list.append(value)
        except (ValueError, TypeError) as e:
            _logger.error('Failed to append an non float value to a symbol entry: {}'.format(e))

    def add_symbol(self, value, obj):
        self.add_value(value)
        self.object_list.append(obj)

    def extend_symbol(self, syme):
        if not isinstance(syme, SymbolEntry):
            _logger.error('Failed to extend a list of a symbol entry. Argument class is {}'.format(syme.__class__.__name__))
            return

        self.value_list.extend(syme.value_list)
        self.object_list.extend(syme.object_list)

    def get_value(self, obj):
        try:
            pos = self.object_list.index(obj)
            return self.value_list[pos]
        except IndexError:
            _logger.error('Invalid list index found for {}'.format(str(obj)))
            return 0

    @property
    def sum(self):
        part_sum = 0
        for v in self.value_list:
            part_sum += v

        return part_sum


class TransitSymbolEntry(SymbolEntry):
    def __init__(self):
        super().__init__()

    @property
    def sum(self):
        part_sum = 0
        if len(self.value_list) > 1:
            for v in self.value_list:
                part_sum += v * 0.95
        elif self.value_list:
            part_sum = self.value_list[0]

        return part_sum
